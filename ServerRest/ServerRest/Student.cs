﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerRest
{
    public class Student
    {
		private string firstname;
		private string lastname;

		// data di compleanno scritta nel formato dd/MM/yyyy
		private string birthday;
		// voti in una stringa separati da una virgola es. 8,5,7,6
		private string grades;

		public string Birthday
		{
			get => birthday;
			set
			{
				string[] s = value.Split('/');
				int month;
				int day;
				int.TryParse(s[0], out day);
				int.TryParse(s[0], out month);
				if (s.Length == 3 && day > 0 && month > 0 && day <= 31 && month <= 12)
					birthday = value;
			}
		}
		public Student(string s)
		{
			try
			{
				string[] studente = s.Split('-');

				firstname = studente[0];
				lastname = studente[1];
				birthday = studente[2];
				grades = studente[3];
			}
			catch
			{
			}
		}

		public Student(string firstname, string lastname, string birthday, string grades)
		{
			this.firstname = firstname;
			this.lastname = lastname;
			this.Birthday = birthday;
			this.grades = grades;
		}

		private string CalcolateAge()
		{
			long birthday = date().Ticks;
			long now = DateTime.Now.Ticks;

			long l = now - birthday;

			DateTime data = new DateTime(l);
			return "Età studente: " + (data.Year - 1) + " anni, " + (data.Month - 1) + " mesi, " + (data.Day - 1) + " giorni. \n";

		}
		private DateTime date()
		{
			string[] birthday = this.birthday.Split('/');
			int giorno = Convert.ToInt16(birthday[0]);
			int mese = Convert.ToInt16(birthday[1]);
			int anno = Convert.ToInt16(birthday[2]);
			return new DateTime(anno, mese, giorno);
		}

		private decimal AverageGrades()
		{
			string[] grades = this.grades.Split(',');
			decimal sum = 0;
			foreach (string s in grades)
				sum += Convert.ToInt16(s);

			return sum / grades.Length;
		}

		public override string ToString()
		{
			string s = "";
			s += firstname + " " + lastname + " nato il " + birthday + "\n";
			s += CalcolateAge();
			s += "La media dei suoi voti è " + AverageGrades() + "\n";
			s += "\n";

			return s;
		}

	}
}
