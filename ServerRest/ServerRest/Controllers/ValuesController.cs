﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ServerRest.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
       [HttpGet]
        public string Get()
        {
			string msg = "";
			foreach (Student s in Program.students)
				msg += s;
            return msg;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
			//value deve essere una stringa del tipo:
			//nome-cognome-gg/MM/yy-voto1,voto2,voto...
        [HttpPost]
        public void Post([FromBody]string value)
        {
			Program.students.Add(new Student(value));
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
