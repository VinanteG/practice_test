﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Alpenite_Test
{	
	class Fibonacci
	{
		private List<BigInteger> sequenza;
		private BigInteger second_last;

		

		public Fibonacci()
		{
			sequenza = new List<BigInteger>();
			sequenza.Add(1);
			sequenza.Add(1);
		}

		public List<BigInteger> Sequenza { get => sequenza;}

		public void InsertNextNum()
		{
			BigInteger last=sequenza.Last();
			sequenza.Add(last + second_last);
			second_last = last;
		}

		public override string ToString()
		{
			string msg="";
			foreach (int n in sequenza)
				msg += n + " ";

				return msg;
		}

	}
}
