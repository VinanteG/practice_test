﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Alpenite_Test
{
	class Program
	{
		static void Main(string[] args)
		{
			Fibonacci f = new Fibonacci();
			Console.WriteLine("Il primo numero di 10000 cifre nella serie di fibonacci è  il "+QuickLoop(f, 1000)+"esimo");
		}
	    static BigInteger QuickLoop(Fibonacci f, BigInteger n) {
			int cont = 1;
			while (ContaCifre(f.Sequenza.Last())<n-100)
			{
				cont+=100;
				for (int i=0;i<100;i++)
					f.InsertNextNum();
			}
			while (ContaCifre(f.Sequenza.Last()) < n )
			{
				cont++;
				f.InsertNextNum();
			}
			return cont;
		}
		static BigInteger ContaCifre(BigInteger n)
		{
			return n.ToString().Length;
		}
	}
}
